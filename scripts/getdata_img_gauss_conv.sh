#!/bin/bash
# downloads data, and runs a python img_gauss_conv script.


# download to the /data folder since entrypoint is /data
#wget -nc -P data https://......./img.fits

# run the code
python3 ../scripts/img_gauss_conv_multiprocessing.py

#python3 ../scripts/img_gauss_conv_dask.py

