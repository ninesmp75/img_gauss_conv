pull-image:
	docker pull registry.gitlab.com/ska-telescope/src/src-workloads/img-gauss-conv:latest

get-data:
	./scripts/get-data.sh

run-task:
	docker run -it --rm --name img-gauss-conv -t -v "${CURDIR}"/scripts:/scripts/ -v "${CURDIR}"/data:/data/ registry.gitlab.com/ska-telescope/src/src-workloads/img-gauss-conv:latest

run-full:
	docker pull registry.gitlab.com/ska-telescope/src/src-workloads/img-gauss-conv:latest
	./scripts/get-data.sh
	docker run -it --rm --name img-gauss-conv -t -v "${CURDIR}"/scripts:/scripts/ -v "${CURDIR}"/data:/data/ registry.gitlab.com/ska-telescope/src/src-workloads/img-gauss_conv:latest


# local build commands:

build-image:
	docker build . --tag img-gauss-conv:latest

build-image-jupyter:
	docker build . -f jupyter/img_gauss_conv_multiproccesing/Dockerfile --tag img-gauss-conv-multi-jhub:latest

run-local:
	./scripts/get-data.sh
	docker run -it --rm --name img-gauss_conv -v "${CURDIR}"/scripts:/scripts/ -v "${CURDIR}"/data:/data/ img-gauss-conv:latest

run-local-jupyter:
	./scripts/get-data.sh
	docker run -it --rm --name img-gauss-conv-multi-jupyter -p 8888:8888 img-gauss-conv-multi-jhub:latest

